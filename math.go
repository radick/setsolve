package main

// SET cards are coordinates in SET affine space
// SET affine space is a 4-dimensional affine space over a three-element field

// GenerateFullStack generates stack (slice) of 81 unique cards
func GenerateFullStack() (stack Stack) {
	var card Card

	for w := 0; w <= 2; w++ {
		for x := 0; x <= 2; x++ {
			for y := 0; y <= 2; y++ {
				for z := 0; z <= 2; z++ {
					card = [4]int{w, x, y, z}
					stack = append(stack, card)
				}
			}
		}
	}

	return stack
}

// Get3rdCard returns 3rd card for card1-card2-card3 set
func Get3rdCard(card1, card2 Card) (card3 Card, setType int) {
	for feature := 0; feature < 4; feature++ {
		if card1[feature] == card2[feature] {
			card3[feature] = card1[feature]
			setType++
		} else {
			card3[feature] = (3 - card1[feature] - card2[feature]) % 3
		}
	}

	return card3, setType
}

// InStack checks if Card is in Stack
func InStack(card Card, stack Stack) bool {
	for _, cardInStack := range stack {
		if card == cardInStack {
			return true
		}
	}
	return false
}

// GetSets calculates and returns sets as a SetsJSON
func GetSets(stack Stack) (setsJSON SetsJSON) {
	stackIndex := Index{}

	for feature := 0; feature < 4; feature++ {
		for _, card1 := range stack {
			for _, card2 := range stack {
				if card1 != card2 {

					card3, setType := Get3rdCard(card1, card2)
					if !InStack(card3, stack) {
						break
					}

					set := Edge{card1, card2, card3}

					// Check if cards of set is in Index
					var dub = false

					for _, card := range set {
						for _, cardInIndex := range stackIndex[card] {
							if cardInIndex[0] == card1 || cardInIndex[1] == card2 ||
								cardInIndex[0] == card2 || cardInIndex[1] == card1 {
								dub = true
								break
							}
							if dub == true {
								break
							}
						}
					}

					if !dub {
						// Add to stackIndex
						stackIndex[card1] = append(stackIndex[card1], [2]Card{card2, card3})

						// Append newly found set to setsJSON
						var setJSON SetJSON
						setJSON.SetType = setType
						setJSON.Set = [3]CardJSON{ConvertCard2CardJSON(card1), ConvertCard2CardJSON(card2), ConvertCard2CardJSON(card3)}
						setsJSON.Sets = append(setsJSON.Sets, setJSON)
					}
				}
			}
		}
	}

	return setsJSON
}
