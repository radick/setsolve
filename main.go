package main

import (
	"log"
	"os"
)

func main() {
	// Output value
	var setsJSON SetsJSON

	if len(os.Args) != 3 {
		log.Fatalf("error processing arguments. Sample usage:\nsetsolve stack_input.json table_sample_sets.json")
	}
	inputFileName := os.Args[1]
	outputFileName := os.Args[2]

	inputTableObjectJSON, err := ReadFile(inputFileName)

	// If input is incorrect - generate full stack
	if err != nil {
		log.Printf("error reading input: %s\nGenerating full stack", err)
		setsJSON := GetSets(GenerateFullStack())
		// 0 is a good ID for full stack sets
		setsJSON.ID = 0

		if err = WriteFile(outputFileName, setsJSON); err != nil {
			log.Panicf("error writing file: %s", err)
		}
		os.Exit(0)
	}

	stack, err, errCardsJSON := ConvertJSON2Math(inputTableObjectJSON)
	if err != nil {
		log.Printf("error while converting JSON: %s\n invalid cards:\n%v", err, errCardsJSON)
		os.Exit(1)
	}
	if len(stack) < 3 {
		log.Printf("error - not enough cards")
		os.Exit(1)
	}

	setsJSON = GetSets(stack)
	if len(setsJSON.Sets) > 0 {
		setsJSON.ID = 0 - inputTableObjectJSON.ID
		if err = WriteFile(outputFileName, setsJSON); err != nil {
			log.Panicf("error writing file: %s", err)
		}
	}
}
