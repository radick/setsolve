setsolve searches sets in [SET game](https://en.wikipedia.org/wiki/Set_(card_game)) in given JSON formatted input

```shell
setsolve stack_input.json sets_sample.json 
```

# Input

**stack_input.json** [sample](./samples/stack_input.json)

You can provide non-exist file to get full stack sets

![](./samples/stack_sample.png)

# Output

**sets_output.json** [sample](./samples/sets_output.json)
