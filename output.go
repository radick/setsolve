package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
)

// WriteFile writes file and returns error if any
func WriteFile(filename string, setsJSON SetsJSON) (err error) {

	if _, err := os.Stat(filename); err == nil {
		return errors.New("file already exists")
	}

	jsonFile, err := json.MarshalIndent(setsJSON, "", " ")
	if err != nil {
		return errors.New("error marshall JSON")
	}

	err = ioutil.WriteFile(filename, jsonFile, 0644)
	if err != nil {
		return errors.New("error writing file")
	}

	return nil
}

// ConvertCard2CardJSON converts card's math representation to JSON
func ConvertCard2CardJSON(card Card) (cardJSON CardJSON) {
	cardJSON.Number = card[0] + 1
	cardJSON.Fill = mapping[0][card[1]]
	cardJSON.Color = mapping[1][card[2]]
	cardJSON.Shape = mapping[2][card[3]]
	return cardJSON
}
