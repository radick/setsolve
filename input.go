package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
)

// ReadFile reads file and returns json representation of table.
func ReadFile(filename string) (stackObjectJSON StackObjectJSON, err error) {
	jsonFile, err := os.Open(filename)
	if err != nil {
		return stackObjectJSON, err
	}
	defer jsonFile.Close()

	bytes, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return stackObjectJSON, err
	}

	err = json.Unmarshal(bytes, &stackObjectJSON)
	if err != nil {
		return stackObjectJSON, err
	}

	return stackObjectJSON, nil
}

// ConvertJSON2Math converts json table representation to mathematical table representation with error checking
// Returns err != nil if any incorrect card given and StackObjectJSON with negative ID and set of incorrect cards
func ConvertJSON2Math(objectJSON StackObjectJSON) (stack Stack, err error, errCardsJSON StackJSON) {

	for _, cardJSON := range objectJSON.Stack {
		var card [4]int
		ok := [3]bool{false, false, false}

		card[0] = cardJSON.Number - 1
		card[1], ok[0] = reverseMappingFill[cardJSON.Fill]
		card[2], ok[1] = reverseMappingColor[cardJSON.Color]
		card[3], ok[2] = reverseMappingShape[cardJSON.Shape]
		if !(ok[0] && ok[1] && ok[2]) || card[0] > 2 || card[0] < 0 {
			errCardsJSON = append(errCardsJSON, cardJSON)
			continue
		}

		stack = append(stack, card)
	}

	if len(errCardsJSON) > 0 {
		return stack, errors.New("incorrect card in the stack (see errCardsJSON)"), errCardsJSON
	}
	return stack, nil, errCardsJSON
}
