package main

// Math

// Card represents card, as well as coordinates in 4D
type Card [4]int

// Stack represents any pile of cards
type Stack []Card

// Edge is line (in 4D) of 3 cards forming a set
type Edge [3]Card

// Index is a map with a Card as a key and array of two Card as a value.
// Value is a list contains pairs of neighbours by set
type Index map[Card][][2]Card

// Input

// CardJSON Card represents card
// Number, Fill, Color and Shape are features
type CardJSON struct {
	Number int    `json:"number"` // 0 - one, 1 - two, 2 - three
	Fill   string `json:"fill"`   // 0 open, 1 striped, 2 solid
	Color  string `json:"color"`  // 0 red, 1 green, 2 purple
	Shape  string `json:"shape"`  // 0 oval, 1 diamond, 2 squiggle
}

// StackJSON represents stack of CardJSON
type StackJSON []CardJSON

// StackObjectJSON represents input JSON file
type StackObjectJSON struct {
	ID    int       `json:"id"`
	Stack StackJSON `json:"stack"`
}

// Reverse mapping from CardJSON to math card
var reverseMappingFill = map[string]int{"open": 0, "striped": 1, "solid": 2}
var reverseMappingColor = map[string]int{"red": 0, "green": 1, "purple": 2}
var reverseMappingShape = map[string]int{"oval": 0, "diamond": 1, "squiggle": 2}

// Output

// SetJSON represents set
// SetType is:
// 1, if cards differ by 1 feature (10% probability)
// 2, if cards differ by 2 features (30% probability)
// 3, if cards differ by 3 features (40% probability)
// 4, if cards differ by 4 features (20% probability)
type SetJSON struct {
	SetType int         `json:"SetType"`
	Set     [3]CardJSON `json:"Set"`
}

// SetsJSON is an output
type SetsJSON struct {
	ID   int       `json:"ID"`
	Sets []SetJSON `json:"Sets"`
}

// Mapping from math card to CardJSON
var mappingFill = [3]string{"open", "striped", "solid"}
var mappingColor = [3]string{"red", "green", "purple"}
var mappingShape = [3]string{"oval", "diamond", "squiggle"}
var mapping = [3][3]string{mappingFill, mappingColor, mappingShape}
